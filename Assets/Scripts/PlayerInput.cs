﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] GameObject PickupRadius;
    [SerializeField] PickUpNew pickupScript;

    public float speed;
    public float moveSpeed;
    public Vector3 forward;
    public Rigidbody RB;
    bool PickingUp;
    // Start is called before the first frame update
    void Start()
    {
        forward = transform.TransformDirection(Vector3.forward);
        RB = gameObject.GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
        

        //if(Input.GetKeyDown(KeyCode.E))
        //{
         //   if(pickupScript.isHolding == false)
        //    {
        //        StartCoroutine(PickUpCheck());
        //    }   
        //    else
        //    {
       //         pickupScript.DropObject();
       //     }
       //     
            
      //  }

        

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        if(Input.GetKey(KeyCode.W))
            {
            RB.velocity = new Vector3(transform.forward.x * moveSpeed, 0f, transform.forward.z * moveSpeed);
        }

        if (Input.GetKey(KeyCode.D))
        {
            RB.velocity = transform.right * moveSpeed;
        }
        if (Input.GetKey(KeyCode.A))
        {
            RB.velocity = -transform.right * moveSpeed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            RB.velocity = new Vector3(-transform.forward.x * moveSpeed, 0f, -transform.forward.z * moveSpeed) ;
        }

        if(!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S))
        {
            RB.velocity = new Vector3 (0,0,0);
        }
        //gameObject.transform.position = new Vector3(transform.position.x + (h * moveSpeed), 0f, transform.position.z + (v * moveSpeed));
        //gameObject.transform.TransformDirection(Vector3.forward);
        //gameObject.transform.position = new Vector3(Input.GetAxis("Horizontal") * moveSpeed *Time.deltaTime, Input.GetAxis("Vertical") * moveSpeed *Time.deltaTime, 0f);


        //how fast the object should rotate

        Vector3 rotation = transform.eulerAngles;

        rotation.x -= Input.GetAxis("Mouse Y") * speed * Time.deltaTime; // Standart Left-/Right Arrows and A & D Keys
        rotation.y += Input.GetAxis("Mouse X") * speed * Time.deltaTime;
        transform.eulerAngles = rotation;
        // transform.Rotate(new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * speed);



        if(Input.GetKeyDown(KeyCode.E))
        {
            if (pickupScript.isHolding == false)
            {
                int layerMask = 1 << 8;
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
                {
                    Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                    PickingUp = true;
                    pickupScript.PickUpObject(hit.transform);
                    hit.transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

                }
            }
            else
            {
                pickupScript.DropObject();
            }


        }




    }

    public IEnumerator PickUpCheck()
    {
        PickupRadius.SetActive(true);
        yield return new WaitForSeconds(1);
        PickupRadius.SetActive(false);

    }
    private void FixedUpdate()
    {
        
    }
}
