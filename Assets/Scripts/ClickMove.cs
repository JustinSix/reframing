﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickMove : MonoBehaviour
{
    public Vector3 mousePosition;
    public float moveSpeed = 0.1f;
    public float zPos;
    public bool Fixed = false;
    public Transform CollisionTransform;
    // Start is called before the first frame update
    void Start()
    {
        zPos = gameObject.transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnMouseDrag()
    {
        if (Fixed == false) 
        {
            Vector3 temp = Input.mousePosition;
            temp.z = zPos + 7.96f; // Set this to be the distance you want the object to be placed in front of the camera.
            this.transform.position = Camera.main.ScreenToWorldPoint(temp);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision");
      if(other.gameObject.tag == gameObject.tag)
        {
            Debug.Log("Transform");
            CollisionTransform = other.gameObject.GetComponent<Transform>();
           
            Fixed = true;
        }
    }
    private void OnMouseUp()
    {
        if(Fixed == true)
        {
            gameObject.transform.position = CollisionTransform.position;
        }
    }
}
