﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpNew : MonoBehaviour
{
    [SerializeField] Transform destination;
    Transform objectsTransformSaved;
    public bool isHolding = false;
    [SerializeField] float dropForce = 0f;

    public void PickUpObject(Transform objectsTransform)
    {
        objectsTransform.parent = destination;
        objectsTransform.position = destination.position;
        objectsTransformSaved = objectsTransform;
        isHolding = true;
    }
    public void DropObject()
    {
        if (isHolding == true)
        {
            isHolding = false;

            objectsTransformSaved.parent = null;
            Rigidbody objectRigidbody = objectsTransformSaved.gameObject.GetComponent<Rigidbody>();
            objectRigidbody.constraints = RigidbodyConstraints.None;
            objectRigidbody.AddForce(destination.forward * dropForce);
        }
    }
}
